# corail_turtlesim
Corail version 1.0, by Benoit Varillon and David Doose
and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

The corail_turtlesim package implements the ROS2 turtlesim using corail.